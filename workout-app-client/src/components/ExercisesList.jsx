import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import Button from '@material-ui/core/Button'

const ExerciseList = () => {
  const [exercises, setExercises] = useState([])

  useEffect(() => {
    axios.get('http://localhost:5000/exercises')
      .then(response => {
        setExercises(response.data)
      })
      .catch(error => console.log(error))
  }, [])

  function deleteExercise (id) {
    axios.delete('http://localhost:5000/exercises/' + id)
      .then(res => console.log(res.data))
    setExercises(exercises.filter(el => el._id !== id))
  }

  console.log(exercises)

  return (
    <>
      <div>Exercise List</div>
      <ul>
        {exercises.map((exercise, index) => {
          return (
            <li key={index}>
              <div><span>Id: </span>{exercise.id}</div>
              <div><span>Username: </span>{exercise.username}</div>
              <div><span>Description: </span>{exercise.description}</div>
              <div><span>Duration: </span>{exercise.duration}</div>
              <div><span>Date: </span>{exercise.date.substring(0, 10)}</div>
              <div><span>Created at: </span>{exercise.createdAt}</div>
              <div><span>Updated at: </span>{exercise.updatedAt}</div>
              <div><Button onClick={() => deleteExercise(exercise._id)}>Delete Exercise</Button></div>
              <div><Link to={'/edit/' + exercise._id}>Edit</Link></div>
              <br />
            </li>
          )
        })}
      </ul>
    </>
  )
}

export default ExerciseList
