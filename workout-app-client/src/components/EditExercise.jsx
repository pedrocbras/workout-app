import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import axios from 'axios'
import DatePicker from 'react-datepicker'

const EditExercise = () => {
  const { id } = useParams()
  const [username, setUsername] = useState('')
  const [description, setDescription] = useState('')
  const [duration, setDuration] = useState(0)
  const [date, setDate] = useState(new Date())
  const [users, setUsers] = useState([])

  useEffect(() => {
    axios.get('http://localhost:5000/exercises/' + id)
      .then(response => {
        setUsername(response.data.username)
        setDescription(response.data.description)
        setDuration(response.data.duration)
        setDate(new Date(response.data.date))
      })
      .catch(error => console.log(error))

    axios.get('http://localhost:5000/users')
      .then(response => {
        if (response.data.length > 0) {
          setUsers(response.data.map(user => user.username))
        }
      })
  }, [])

  function onChangeUsername (event) {
    setUsername(event.target.value)
  }

  function onChangeDescription (event) {
    setDescription(event.target.value)
  }

  function onChangeDuration (event) {
    setDuration(event.target.value)
  }

  function onChangeDate (date) {
    setDate(date)
  }

  function onSubmit (event) {
    event.preventDefault()
    const exercise = {
      username: username,
      description: description,
      duration: duration,
      date: date
    }

    console.log(exercise)

    axios.post('http://localhost:5000/exercises/update/' + id, exercise)
      .then(res => console.log(res.data))

    window.location = '/'
  }

  return (
    <div>
      <span>Edit Exercise Log</span>
      <form onSubmit={onSubmit}>
        <label>Username: </label>
        <select
          required
          value={username}
          onChange={onChangeUsername}>
          {
            users.map(user => {
              return (
                <option key={user} value={user}>{user}</option>
              )
            })
          }
        </select>
        <label> Description: </label>
        <input
          type="text"
          value={description}
          onChange={onChangeDescription}
        />
        <label> Duration: </label>
        <input
          type="text"
          value={duration}
          onChange={onChangeDuration}
        />
        <label>Date: </label>
        <DatePicker
          selected={date}
          onChange={onChangeDate}
        />
        <input type={'submit'} value={'Edit Exercise'} />
      </form>
    </div>
  )
}

export default EditExercise
