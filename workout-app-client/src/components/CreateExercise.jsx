import React, { useState, useEffect } from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import axios from 'axios'

const CreateExercise = () => {
  const [username, setUsername] = useState('')
  const [description, setDescription] = useState('')
  const [duration, setDuration] = useState(0)
  const [date, setDate] = useState(new Date())
  const [users, setUsers] = useState([])

  useEffect(() => {
    axios.get('http://localhost:5000/users')
      .then(response => {
        if (response.data.length > 0) {
          setUsers(response.data.map(user => user.username))
          setUsername(response.data[0].username)
        }
      })
  }, [])

  function onChangeUsername (event) {
    setUsername(event.target.value)
  }

  function onChangeDescription (event) {
    setDescription(event.target.value)
  }

  function onChangeDuration (event) {
    setDuration(event.target.value)
  }

  function onChangeDate (date) {
    setDate(date)
  }

  function onSubmit (event) {
    event.preventDefault()
    const exercise = {
      username: username,
      description: description,
      duration: duration,
      date: date
    }

    console.log(exercise)

    axios.post('http://localhost:5000/exercises/add', exercise)
      .then(res => console.log(res.data))

    window.location = '/'
  }

  return (
    <div>
      <span>Create New Exercise</span>
      <form onSubmit={onSubmit}>
        <label>Username: </label>
        <select
          required
          value={username}
          onChange={onChangeUsername}>
          {
            users.map(user => {
              return (
                <option key={user} value={user}>{user}</option>
              )
            })
          }
        </select>
        <label> Description: </label>
        <input
          type="text"
          value={description}
          onChange={onChangeDescription}
        />
        <label> Duration: </label>
        <input
          type="text"
          value={duration}
          onChange={onChangeDuration}
        />
        <label>Date: </label>
        <DatePicker
          selected={date}
          onChange={onChangeDate}
        />
        <input type={'submit'} value={'Create Exercise'} />
      </form>
    </div>
  )
}

export default CreateExercise
