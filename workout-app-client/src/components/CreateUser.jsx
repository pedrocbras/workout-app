import React, { useState } from 'react'
import axios from 'axios'

const CreateUser = () => {
  const [username, setUsername] = useState('')

  function onChangeUsername (event) {
    setUsername(event.target.value)
  }

  function onSubmit (event) {
    event.preventDefault()

    const user = {
      username: username
    }

    // console.log(user)

    axios.post('http://localhost:5000/users/add', user)
      .then(res => console.log(res.data))

    setUsername('')
  }

  return (
    <>
      <div>Create User</div>
      <form onSubmit={onSubmit}>
        <label>Username: </label>
        <input
          type={'text'}
          required
          value={username}
          onChange={onChangeUsername}
        />
        <input type='submit' value={'Create User'}/>
      </form>
    </>
  )
}

export default CreateUser
