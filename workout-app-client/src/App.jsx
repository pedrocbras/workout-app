import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
// import { makeStyles } from '@material-ui/styles'
import Navbar from './components/Navbar'
import ExercisesList from './components/ExercisesList'
import EditExercise from './components/EditExercise'
import CreateExercise from './components/CreateExercise'
import CreateUser from './components/CreateUser'

// const useStyles = makeStyles(theme => ({
//   test: {
//     color: theme.colors.main,
//     fontFamily: theme.font
//   }
// }))

const App = () => {
  // const classes = useStyles()
  return (
    <Router>
      <Navbar />
      <Route path='/' exact component={ExercisesList} />
      <Route path='/edit/:id' component={EditExercise} />
      <Route path='/create' component={CreateExercise} />
      <Route path='/user' component={CreateUser} />
    </Router>
  )
}

export default App
