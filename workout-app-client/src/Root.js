import React from 'react'
import { JssProvider } from 'react-jss'
import { create as createJss } from 'jss'
import { createGenerateClassName, jssPreset, ThemeProvider } from '@material-ui/styles'
import App from './App'
import theme from './theme'

const generateClassName = createGenerateClassName()

const jss = createJss({
  ...jssPreset(),
  // We define a custom insertion point that JSS will look
  // for injecting the Material-UI styles in the DOM.
  insertionPoint: 'jss-insertion-point'
})

const Root = () => {
  return (
    <JssProvider jss={jss} generateClassName={generateClassName}>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </JssProvider>
  )
}

export default Root
