import { createMuiTheme } from '@material-ui/core/styles'

export default createMuiTheme({
  colors: {
    main: 'red'
  },
  font: 'roboto'
})
