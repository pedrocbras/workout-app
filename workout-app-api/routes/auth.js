const router = require('express').Router()
const Account = require('../models/account.model')

// VALIDATION
const Joi = require('@hapi/joi')

const schema = Joi.object({
  name: Joi.string().min(6).required(),
  email: Joi.string().min(6).required().email(),
  password: Joi.string().min(6).required()
})

router.route('/register').post((req, res) => {
  const { error } = schema.validate(req.body)
  if (error) return res.status(400).send(error.details[0].message)

  const account = new Account({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  })

  account.save()
    .then(() => res.json('Account added!'))
    .catch(err => res.status(400).json('Error: ' + err))
})

module.exports = router
