const mongoose = require('mongoose')

const accountSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    min: 3
  },
  email: {
    type: String,
    required: true,
    min: 5
  },
  password: {
    type: String,
    required: true,
    min: 6
  },
  date: {
    type: Date,
    default: Date.now
  }
})

const Account = mongoose.model('Account', accountSchema)

module.exports = Account
